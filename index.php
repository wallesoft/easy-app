<?php
//debug
define('E_DEBUG',true);
//config
$config = require(__DIR__.'/config/main.php');
//vendor
require(__DIR__.'/vendor/autoload.php');
require(__DIR__.'/vendor/wallesoft/easy/framework/Easy.php');

//run
(new easy\web\Application($config))->run();
