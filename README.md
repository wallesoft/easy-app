# easy-app

#### 介绍
easy framework 初始化示例代码
A Demo hello World;;;
#### 安装教程
##### Composer

` composer create-project wallesoft/easy-app demo --prefer-dist `

##### Git && Composer

'git clone https://gitee.com/wallesoft/easy-app.git' && composer install -vvv `

clone 源码目录下执行 composer install -vvv 由于通过主站 packagist.org 安装较慢(你懂的)
特别在配置中添加源为 https://packagist.jp ，如果有需要请在 composer.json中修改
`
"repositories": {
    "packagist": {
        "type": "composer",
        "url": "https://packagist.jp"
    }
}
`
##### 下载easy-app && easy framework 手动配置安装
通过   https://gitee.com/wallesoft/easy.git 下载 easy
然后   https://gitee.com/wallesoft/easy-app.git 下载easy-app
手动修改 easy-app 中 index.php easy framework 的引入路径即可

require(dirname(__FILE__).'where is easy!!!!'); 
