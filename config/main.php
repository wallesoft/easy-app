<?php
//config demo
return [
    'appPath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'/../',
    'components' => [
        //db
        'db' => [
            'class' => 'easy\db\mysql\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=demo',
            'username' => 'demo',
            'password' => 'demo',
            'charset' => 'utf8', //default
            'tablePrefix' => 'demo_' // 表前缀
        ],
        //cache 缓存 存放在文件中
        'cache' => [
            'class' => 'easy\caching\FileCache',
            'path' => dirname(__FILE__).'/../data/cache',
        ],
        //日志
        'log' => [
            'class' => 'easy\helper\Log',
            'savePath' => dirname(__FILE__).'/../data/log',
        ],
        //session
        'session' => [
            'isOpen' => true, // Default is false
            'savePath' => dirname(__FILE__).'/../data/session',
        ],
        //redis
        'redis' => [
            'class' => 'easy\db\redis\Connection',
            'hostname' => '127.0.0.1',
            'password' => 'demo',// redis service config file -> requirepass = 'xxxx'
        ],
        //others

    ],
    'modules' => [],
];
